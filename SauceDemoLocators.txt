#найти все локаторы для функциональных элементов, т.е. иконки, кнопки, поля ввода и т.д
#найти локаторы для окна Login


LOCATORS:
ID
NAME
LinkText, PartialLinkText
TagName
ClassName
CssSelector
XPATH

  All locators for elements for website https://www.saucedemo.com/

-Locators for main site icon:
driver.find_element(By.TAG_NAME, "div.login_logo")
driver.find_element(By.CLASS_NAME, "login_logo")
driver.find_element(By.CSS_SELECTOR, "div.login_logo")
driver.find_element(By.XPATH, "//*[@id='root']/div/div[1]")

-Locators for icon with a picture:
driver.find_element(By.TAG_NAME, "div.bot_column")
driver.find_element(By.CLASS_NAME, "bot_column")
driver.find_element(By.CSS_SELECTOR, "div.bot_column")
driver.find_element(By.XPATH, "//*[@id='root']/div/div[2]/div[1]/div[2]")

-Locators for input field "Username":
driver.find_element(By.ID, "user-name")
driver.find_element(By.NAME, "user-name")
driver.find_element(By.TAG_NAME, "input.Username")
driver.find_element(By.CLASS_NAME, "input_error form_input")
driver.find_element(By.CSS_SELECTOR, "#user-name")
driver.find_element(By.XPATH, "//*[@id='user-name']")

-Locators for input field "Password":
driver.find_element(By.ID, "password")
driver.find_element(By.NAME, "password")
driver.find_element(By.TAG_NAME, "input.input_error form_input")
driver.find_element(By.CLASS_NAME, "input_error form_input")
driver.find_element(By.CSS_SELECTOR, "#password")
driver.find_element(By.XPATH, "//*[@id='password']")

-Locators  for field "Login":
driver.find_element(By.ID, "login-button")
driver.find_element(By.NAME, "login-button")
driver.find_element(By.TAG_NAME, "input.submit")
driver.find_element(By.CLASS_NAME, "submit-button btn_action")
driver.find_element(By.CSS_SELECTOR, "#login-button")
driver.find_element(By.XPATH, "//*[@id='login-button']")

-Locator LinkText for window with possible password for all users:
driver.find_element(By.LINK_TEXT, "Password for all users:")

-Locator PartialLinkText for window with possible accepted usernames:
driver.find_element(By.PARTIAL_LINK_TEXT, "Accepted usernames")
